import React from 'react'
import './Letters.css'

const COLORS = ["white", "red", "orange", "yellow", "green", "blue", "violet"]
const MAX_SIZE = 300
const MIN_SIZE = 10

export default class Letters extends React.Component {
  state = {
    letters: "",
    colorIndex: 0,
    fontSize: 32,
  }

  componentDidMount() {
    document.addEventListener('keydown', this.handleKey)
  }

  componentWillUnmount() {
    document.removeEventListener('keydown', this.handleKey)
  }

  scroll() {
    if (this.bottomEl) {
      this.bottomEl.scrollIntoView()
    }
  }

  cycleColor(n) {
    let index = (this.state.colorIndex + n) % COLORS.length
    if (index < 0) {
      index = COLORS.length - 1
    }
    this.setState({ colorIndex: index })
  }

  cycleSize(n) {
    let size = (this.state.fontSize * n)
    if (size > MAX_SIZE) {
      size = MAX_SIZE
    }
    if (size < MIN_SIZE) {
      size = MIN_SIZE
    }
    this.setState({ fontSize: size })
  }

  handleKey = ev => {
    ev.preventDefault()
    const key = ev.key
    let letters = this.state.letters

    if (key === 'ArrowRight') {
      this.cycleColor(1)
    } else if (key === 'ArrowLeft') {
      this.cycleColor(-1)
    } else if (key === 'ArrowUp') {
      this.cycleSize(1.2)
    } else if (key === 'ArrowDown') {
      this.cycleSize(1/1.2)
    } else if (key === 'Backspace') {
      letters = letters.substr(0, letters.length - 1)
      this.setState({ letters })
    } else if (key.length === 1) {
      letters += ev.key
      this.setState({ letters })
    }
    this.scroll()
  }

  render() {
    const style = {
      color: COLORS[this.state.colorIndex],
      fontSize: `${this.state.fontSize}pt`
    }

    return (
      <div>
        <div className="letters" style={style}>
          {this.state.letters.split('').map((letter, index) => (
            <span key={index}>{letter}</span>
          ))}
          _
        </div>
        <div ref={el => this.bottomEl = el}>
        </div>
      </div>
    )
  }
}
