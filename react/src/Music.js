import React, { Component } from 'react';
import './Music.css';

const PlaylistTitle = ({ playlists, active, page, pages }) => {
  if (playlists) {
    if (active) {
      const playlist = playlists.find(pl => pl.uri === active)
      const pagination = pages > 1 ? `${page + 1}/${pages}` : ''
      console.log({ playlists, active, playlist })
      return (
        <h4>
          {playlist && playlist.name} {pagination}
        </h4>
      )
    }
  }
  return null
}

class Music extends Component {
  state = {
    playlists: null,
    activePlaylist: null,
    tracks: null,
    activeTlTrack: null,
    page: 0,
    perPage: 26,
    pages: 0,
    state: null,
  }

  componentDidMount() {
    this.mopidy = new window.Mopidy({
      webSocketUrl: "ws://192.168.1.150:6680/mopidy/ws/"
    })
    window.mopidy = this.mopidy
    this.mopidy.on(console.log.bind(console));  // Log all events
    this.mopidy.on("state:online", async () => {
      const [ playlists, tracks, state ] = await Promise.all([
        this.mopidy.playlists.getPlaylists(),
        this.mopidy.tracklist.getTlTracks(),
        this.mopidy.playback.getState()
      ])
      console.log({ playlists, tracks, state })
      this.setState({ playlists: playlists.reverse(), tracks, state })
      this.markCurrent()
    })

    this.mopidy.on("event:tracklistChanged", async (x) => {
      console.log('tracklistChanged', { x })
      const tracks = await this.mopidy.tracklist.getTlTracks()
      console.log({ tracks })
      this.setState({ tracks })
    })
    
    this.mopidy.on("event:playbackStateChanged", ({ old_state, new_state }) => {
      console.log('--------------------------------', { old_state, new_state })
      this.markCurrent()
      this.setState({ state: new_state })
    })

    document.onkeypress = this.handleKey
  }

  markCurrent = async () => {
    this.mopidy.playback.getCurrentTlTrack()
        .then(current => this.setState({ activeTlTrack: current && current.tlid }))
  }

  handleKey = async (event) => {
    const keyCode = event.charCode

    console.log({ keyCode, event })

    if (keyCode === ' '.charCodeAt()) {
      if (this.state.state === 'playing') {
        this.mopidy.playback.pause()
      }
      if (this.state.state === 'paused') {
        this.mopidy.playback.resume()
      }
    }

    if (this.state.playlists && keyCode >= 48 && keyCode <= 57) {
      const i = keyCode - 48
      if (i < this.state.playlists.length) {
        const playlist = this.state.playlists[i]
        if (playlist) {
          if (this.state.activePlaylist === playlist.uri) {
            if (this.state.tracks) {
              this.setState({ page: (this.state.page + 1) % Math.ceil(this.state.tracks.length / this.state.perPage )})
            }
          } else {
            this.mopidy.tracklist.clear()
            this.setState({ activePlaylist: playlist.uri, tracks: null, page: 0 })
            if (playlist.tracks) {
              this.mopidy.tracklist.add(playlist.tracks)
            }
          }
        }
      }
    }

    if (this.state.tracks && keyCode >= 'a'.charCodeAt() && keyCode <= 'z'.charCodeAt()) {
      const i = keyCode - 'a'.charCodeAt() + this.state.page * this.state.perPage
      console.log({i})
      if (i < this.state.tracks.length) {
        const track = this.state.tracks[i]
        console.log('playing', track)
        this.mopidy.playback.play(track)
      }
    }
  }

  render() {
    return (
      <div className="outer">
        <div className="inner">
          <div className="content">
            <h1>DJ RUBY {this.state.state}</h1>

            <div className="playlists">
              {this.state.playlists ?
               this.state.playlists.map((playlist, i) => {
                 const active = this.state.activePlaylist === playlist.uri ? 'active' : ''
                 return (
                   <span key={playlist.uri} className="item">
                     <span className={active}>&nbsp;{i}&nbsp;</span>
                   </span>
                 )
               }) : (
                 <div>LOADING PLAYLISTS</div>
               )}
            </div>

            <PlaylistTitle
              playlists={this.state.playlists}
              active={this.state.activePlaylist}
              page={this.state.page}
              pages={this.state.tracks && Math.ceil(this.state.tracks.length / this.state.perPage)}
            />

            <div className="tracks">
              {this.state.tracks ?
               this.state.tracks.slice(this.state.page * this.state.perPage, this.state.page * this.state.perPage + this.state.perPage).map((tlTrack, i) => (
                 <div key={tlTrack.tlid} className="item">
                   <div className={this.state.activeTlTrack === tlTrack.tlid ? 'active' : ''}>
                     <code>{String.fromCharCode(65 + i)}</code>: <b>{tlTrack.track.name}</b> :: {tlTrack.track.album.artists[0].name}
                   </div>
                 </div>
               )) : (
                 <div>LOADING TRACKS</div>
               )
              }
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Music;
