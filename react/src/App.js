import React from 'react'
import { Route, Switch, withRouter } from 'react-router-dom'
//import Music from './Music'
import Letters from './Letters'

class App extends React.Component {
  componentDidMount() {
    document.addEventListener('keydown', this.handleKey)
  }

  componentWillUnmount() {
    document.removeEventListener('keydown', this.handleKey)
  }

  handleKey = ev => {
    if (ev.key === 'Escape') {
      if (this.props.location.pathname === '/letters') {
        this.props.history.push('/music')
      } else {
        this.props.history.push('/letters')
      }
      console.log(this.props)
    }
  }

  render() {
    return (
      <Switch>
        {/* <Route path="/" exact component={Music} />
            <Route path="/music" component={Music} /> */}
        <Route path="/" component={Letters} />
      </Switch>
    )
  }
}

export default withRouter(App)
