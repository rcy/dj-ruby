run:
	cd app && yarn start

build:
	cd react && rm -rf build && yarn build

run2: electron/electron build
	electron/electron react/build/index.html

# https://electronjs.org/docs/tutorial/application-distribution
electron/electron:
	mkdir -p electron
	cd electron && curl -L https://github.com/electron/electron/releases/download/v3.0.10/electron-v3.0.10-linux-x64.zip --output - | busybox unzip -
	chmod +x $@

dist-clean:
	rm -rf ./electron
